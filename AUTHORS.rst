=======
Authors
=======

`pyMETHES` was written by:

* Alise Chachereau <alisec@ethz.ch>, and
* Jansen Kerry <kjansen@student.ethz.ch>, and
* Niese Markus <mniese@student.ethz.ch>

Contributors:

 * Mikołaj Rybiński <mikolaj.rybinski@id.ethz.ch>
