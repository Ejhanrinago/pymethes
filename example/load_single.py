"""
Example script to load, plot or save simulation results.
"""

import pickle

# loading a simulation saved as a pickle:
with open("results/Ar_N2.pickle", "rb") as pickle_file:
    sim = pickle.load(pickle_file)

# available plots:
sim.electrons.plot_position(block=False)
sim.electrons.plot_velocity(block=False)
sim.electrons.plot_energy(block=False)
sim.output.plot_temporal_evolution(block=False)
sim.output.plot_energy_distribution(block=True)

# saving output data in json format:
sim.output.save_temporal_evolution()
sim.output.save_swarm_parameters()
sim.output.save_energy_distribution()
