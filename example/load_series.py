"""
Example script to load and plot a series of simulation results.
"""

import matplotlib.pyplot as plt
import pandas as pd
import glob
import json

# load the files to a pandas DataFrame
files = glob.glob('results/CO2_*_swarm_parameters.json')
df = None
for i, f in enumerate(files):
    with open(f, "r") as json_file:
        res = json.load(json_file)
    if df is None:
        df = pd.DataFrame(res, index=[i])
    else:
        df = df.append(pd.DataFrame(res, index=[i]))

# plot the swarm parameters versus E/N
fig = plt.figure()

ax = fig.add_subplot(221)
ax.errorbar(df['E/N (Td)'], df['bulk drift velocity (m.s-1)'],
            yerr=df['bulk drift velocity error (m.s-1)'], label='bulk')
ax.errorbar(df['E/N (Td)'], df['flux drift velocity (m.s-1)'],
            yerr=df['flux drift velocity error (m.s-1)'], label='flux')
ax.set_xlabel('E/N (Td)')
ax.set_ylabel('w (m s$^{-1}$)')
ax.legend()

ax = fig.add_subplot(222)
ax.errorbar(df['E/N (Td)'], df['bulk L diffusion coeff. * N (m-1.s-1)'],
            yerr=df['bulk L diffusion coeff. error * N (m-1.s-1)'], label='bulk')
ax.errorbar(df['E/N (Td)'], df['flux L diffusion coeff. * N (m-1.s-1)'],
            yerr=df['flux L diffusion coeff. error * N (m-1.s-1)'], label='flux')
ax.set_xlabel('E/N (Td)')
ax.set_ylabel('ND$_L$ (m$^{-1}$ s$^{-1}$)')
ax.legend()

ax = fig.add_subplot(223)
ax.errorbar(df['E/N (Td)'], df['bulk T diffusion coeff. * N (m-1.s-1)'],
            yerr=df['bulk T diffusion coeff. error * N (m-1.s-1)'], label='bulk')
ax.errorbar(df['E/N (Td)'], df['flux T diffusion coeff. * N (m-1.s-1)'],
            yerr=df['flux T diffusion coeff. error * N (m-1.s-1)'], label='flux')
ax.set_xlabel('E/N (Td)')
ax.set_ylabel('ND$_T$ (m$^{-1}$ s$^{-1}$)')
ax.legend()

ax = fig.add_subplot(224)
ax.plot(df['E/N (Td)'], df['effective ionization rate coeff. (convolution) (m3.s-1)'],
        label='effective')
ax.plot(df['E/N (Td)'], df['ionization rate coeff. (convolution) (m3.s-1)'],
        label='ionization')
ax.plot(df['E/N (Td)'], df['attachment rate coeff. (convolution) (m3.s-1)'],
        label='attachment')
ax.set_xlabel('E/N (Td)')
ax.set_ylabel('k (m s$^{-3}$)')
ax.legend()

plt.show()
