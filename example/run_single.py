"""
Example script to run a simulation with a single set of settings.
"""

from pyMETHES import Simulation

# load the settings
sim = Simulation("config/Ar_N2.json5")

# run the simulation
sim.run()
