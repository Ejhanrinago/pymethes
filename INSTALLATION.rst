.. highlight:: shell

============
Installation
============


Stable release
--------------

To install pyMETHES, run this command in your terminal:

.. code-block:: console

    $ pip install pymethes

This is the preferred method to install pyMETHES, as it will always install
the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for pyMETHES can be downloaded from the `GitLab repo`_.

You can either clone the repository:

.. code-block:: console

    $ git clone git@gitlab.com:ethz_hvl/pymethes.git

Or download the `tarball`_:

.. code-block:: console

    $ curl -OL https://gitlab.com/ethz_hvl/pymethes/-/archive/master/pymethes.tar.gz

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _GitLab repo: https://gitlab.com/ethz_hvl/pymethes
.. _tarball: https://gitlab.com/ethz_hvl/pymethes/-/archive/master/pymethes.tar.gz