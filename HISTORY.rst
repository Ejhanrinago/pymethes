=======
History
=======

0.1.0 (2020-06-19)
------------------

* Simulation in homogeneous electric field
* Parametric sweep over one configuration parameter
