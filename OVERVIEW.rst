========
Overview
========

Class diagram:

.. image:: class_diagram.svg


Simulation flow diagram:


.. image:: flow_diagram.svg

..
    should be: .. mermaid:: flow_diagram.mmd
    but RTD produces raw output despite conf.py stating
    mermaid_output_format = 'png'
..
