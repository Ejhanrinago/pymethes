=====
Usage
=====

To use pyMETHES in a project::

    import pyMETHES

    sim = pyMETHES.Simulation('config.json')
    sim.run()


See more `examples`_

.. _examples: https://gitlab.com/ethz_hvl/pymethes/-/tree/master/example
