Welcome to pyMETHES documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   usage
   overview
   modules
   contributing
   authors
   history

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
