#  Copyright (c) 2020 ETH Zurich

"""
Tests for the CountedRates and ConvolutedRates classes.
"""

# Import Packages
import pytest
import numpy as np
import scipy.interpolate

# Import modules
from pyMETHES.rate_coefficients import ConvolutedRates, CountedRates
from pyMETHES.energy_distribution import TimeAveragedEnergyDistribution
from pyMETHES.gas_mixture import GasMixture
from pyMETHES.config import Config
from pyMETHES.temporal_evolution import TimeSeries
from pyMETHES.electrons import Electrons

np.seterr(all='raise')


@pytest.fixture(scope='module')
def gas_mixture():
    config = Config('tests/data/config/test_config.json5')
    gm = GasMixture(config.gases,
                    config.paths_to_cross_section_files,
                    config.fractions,
                    config.max_cross_section_energy)
    return gm


@pytest.fixture(scope='module')
def distribution():
    distri = TimeAveragedEnergyDistribution()
    bins = np.linspace(0, 10, 6)
    distri.energy_bins = bins
    distri.calculate_bin_centers()
    distri.collect_histogram(np.repeat(distri.energy_bin_centers, 10))
    distri.calculate_distribution(np.arange(10))
    return distri


@pytest.fixture(scope='module')
def time_series_exponential():
    ts = TimeSeries(Electrons(1000, [0, 0, 0], [1, 1, 0], 3))
    ts.ind_equ = 0
    ts.time = np.array(range(100))*0.1
    ts.num_electrons = np.exp(ts.time)
    ts.num_anions = 0.5*ts.num_electrons
    ts.num_cations = 0.7*ts.num_electrons
    return ts


@pytest.fixture(scope='module')
def time_series_linear():
    ts = TimeSeries(Electrons(1000, [0, 0, 0], [1, 1, 0], 3))
    ts.ind_equ = 0
    ts.time = np.array(range(100))*0.1
    ts.num_electrons = ts.time
    ts.num_anions = 0.5*ts.num_electrons
    ts.num_cations = 0.7*ts.num_electrons
    return ts


def test_instantiation():
    CountedRates(1, False)
    ConvolutedRates()


def test_calculate_convolution(distribution):
    x = np.array(range(25))
    y = x ** 2
    interp = scipy.interpolate.interp1d(x, y)
    assert np.isclose(ConvolutedRates.calculate_convolution(interp, distribution),
                      52801283.617982395)


def test_convoluted_calculate_data(gas_mixture, distribution):

    rates = ConvolutedRates()
    rates.calculate_data(gas_mixture, distribution)
    assert np.isclose(rates.ionization, 2.891144623543668e-14)
    assert np.isclose(rates.attachment, 8.813075700358316e-15)
    assert np.isclose(rates.effective, 2.0098370535078364e-14)


def test_counted_calculate_data(time_series_exponential, time_series_linear):

    rates = CountedRates(1, False)
    rates.calculate_data(time_series_exponential)
    assert np.isclose(rates.ionization, 0.7)
    assert np.isclose(rates.attachment, 0.5)
    assert np.isclose(rates.effective, 1)

    rates = CountedRates(1, True)
    rates.calculate_data(time_series_linear)
    assert np.isclose(rates.ionization, 0.7)
    assert np.isclose(rates.attachment, 0.5)
    assert np.isclose(rates.effective, 1)
