#  Copyright (c) 2020 ETH Zurich

"""
Tests for the utils module.
"""

# Import Packages
import numpy as np
import scipy.constants as csts

# Import modules
import pyMETHES.utils as utils


def test_velocity_from_energy():

    energy = np.array([0, 12, 37])
    assert (utils.velocity_from_energy(energy) == np.sqrt(
        (2 * energy * csts.elementary_charge) / csts.electron_mass)).all()


def test_energy_from_velocity():

    velocity = np.array([0, 12, 37])
    assert (utils.energy_from_velocity(velocity) ==
            0.5 * csts.electron_mass * velocity ** 2 / csts.elementary_charge).all()


def test_acceleration_from_electric_field():

    e_field = np.array([-1, 0, 3])

    acceleration = e_field * csts.elementary_charge / csts.electron_mass
    assert (utils.acceleration_from_electric_field(e_field) == acceleration).all()
