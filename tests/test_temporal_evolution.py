#  Copyright (c) 2020 ETH Zurich

"""
Tests for the TimeSeries class.
"""

# Import Packages
import pytest
import numpy as np
import pandas as pd

# Import modules
from pyMETHES.electrons import Electrons
from pyMETHES.temporal_evolution import TimeSeries

np.seterr(all='raise')


@pytest.fixture(scope='module')
def time_series():
    electrons = Electrons(1000, [0, 0, 0], [1, 1, 1], 1)
    ts = TimeSeries(electrons)
    return ts


def test_instantiation():
    electrons = Electrons(1000, [0, 0, 0], [1, 1, 1], 1)
    TimeSeries(electrons)


def test_time_series_append(time_series):
    ts = time_series
    electrons = Electrons(1000, [0, 0, 0], [1, 1, 1], 1)
    ts.append_data(electrons, 1, 1, 1, 1)
    assert all(ts.time == [0, 1])
    for i in range(3):
        assert all(np.isclose(ts.mean_position[:, i], 0, atol=0.2))
        assert all(np.isclose(ts.var_position[:, i], 1, atol=0.2))
        assert all(np.isclose(ts.mean_velocity[:, i], 0))
        assert all(np.isclose(ts.mean_velocity_moment[:, i], 0))
    assert all(np.isclose(ts.mean_energy, 0))
    assert all(ts.num_electrons == [1000, 1000])
    assert all(ts.num_anions == [0, 1])
    assert all(ts.num_cations == [0, 1])
    assert all(ts.num_collisions == [0, 1])


def test_to_dataframe(time_series):
    df = time_series.to_dataframe()
    assert isinstance(df, pd.DataFrame)
    assert df.shape == (2, 19)
