#  Copyright (c) 2020 ETH Zurich

"""
Test to validate the simulation output, using the Maxwell model at E/N = 1 Td,
 and T = 0 K. The results are compared to calculations using theoretical formulae from
 K. F. Ness, J. Phys. D: Appl. Phys. 27 (1994) 1848-1861.
"""

# Import Packages
import pytest
import numpy as np
import scipy.constants as csts

# Import modules
from pyMETHES.simulation import Simulation

np.seterr(all='raise')


@pytest.mark.validation
def test_maxwell():
    sim = Simulation('tests/data/config/maxwell.json5')
    mass_ratio = 2 * csts.electron_mass / (4 * csts.proton_mass)
    sim.gas_mixture.mass_ratios = np.array([mass_ratio])

    sim.run()
    assert np.isclose(sim.output.energy_distribution.energy_mean,
                      0.5064, atol=0, rtol=0.03)
    assert all(np.isclose(sim.output.flux.w, [0, 0, 4.943e3], atol=150, rtol=0.01))
    assert all(np.isclose(sim.output.flux.DN, 1.668e24, atol=0, rtol=0.06))
