#  Copyright (c) 2020 ETH Zurich

"""
Tests for the Simulation class.
"""

# Import Packages
import pytest
import numpy as np
import copy

# Import modules
from pyMETHES.simulation import Simulation

np.seterr(all='raise')


@pytest.fixture(scope='module')
def sim(tmp_path_factory):
    sim = Simulation('tests/data/config/test_config.json5')
    sim.config.output_directory = str(tmp_path_factory.mktemp('results'))
    single = np.arange(1, 101)
    triple = np.ones((100, 3))
    sim.output.time_series.time = single
    sim.output.time_series.mean_position = triple
    sim.output.time_series.var_position = triple
    sim.output.time_series.mean_velocity = triple
    sim.output.time_series.mean_velocity_moment = triple
    sim.output.time_series.mean_energy = single
    sim.output.time_series.std_energy = single
    sim.output.time_series.num_electrons = single
    sim.output.time_series.num_collisions = single
    sim.output.time_series.num_anions = single
    sim.output.time_series.num_cations = single
    sim.output.energy_distribution.generate_bins(
        sim.config.num_energy_bins, 5.3)
    return sim


def test_instantiation():
    Simulation('tests/data/config/test_config.json5')


def test_apply_config(sim):
    sim = copy.deepcopy(sim)
    cfg_dict = sim.config.to_dict()
    cfg_dict['output']['output_directory'] += 'new_directory/'
    sim.apply_config(cfg_dict)
    sim.save_pickle()


def test_save_pickle(sim):
    sim.save_pickle()


def test_advance_one_step(sim):
    sim.mc.calculate_max_coll_freq(sim.gas_mixture)
    sim.advance_one_step()


def test_collect_output_data(sim):
    sim.output.time_series.ind_equ = None
    sim.collect_output_data(1, 1, 1, 1)
    assert sim.output.time_series.ind_equ is not None
    sim.output.time_series.ind_equ = 0
    sim.collect_output_data(1, 1, 1, 1)


def test_end(sim):
    sim.output.flux.w = np.ones((3,)) * np.nan
    sim.output.flux.DN = np.ones((3,)) * np.nan
    sim.output.flux.w_err = np.zeros((3,)) * np.nan
    sim.output.flux.DN_err = np.zeros((3,)) * np.nan
    assert not sim.end_simulation()

    # condition on the convergence of flux w and flux DN
    sim.output.flux.w = np.ones((3,))
    sim.output.flux.DN = np.ones((3,))
    sim.output.flux.w_err = np.zeros((3,))
    sim.output.flux.DN_err = np.zeros((3,))
    assert sim.end_simulation()
    sim.output.flux.w_err = np.ones((3,))
    sim.output.flux.DN_err = np.ones((3,))
    assert not sim.end_simulation()

    # no electrons
    sim.electrons.position = np.empty((3, 0))
    assert sim.end_simulation()

    # too many electrons
    sim.config.conserve = False
    sim.config.num_e_max = 100
    sim.electrons.position = np.empty((3, 100))
    assert not sim.end_simulation()
    assert sim.config.conserve

    sim.output.time_series.num_collisions[-1] = sim.config.num_col_max
    assert sim.end_simulation()


def test_calculate_final_output(sim):
    sim.output.time_series.ind_equ = 0
    sim.output.energy_distribution.generate_bins(
        sim.config.num_energy_bins,
        1.2
    )
    sim.calculate_final_output()


def test_run(sim):
    sim.config.EN = 0
    sim.apply_config()
    sim.run()


def test_run_series(sim):
    sim.config.EN = 0
    sim.apply_config()
    values = np.array([1e3, 1e5])
    sim.run_series('pressure', values)
    with pytest.raises(ValueError):
        sim.run_series('invalid_parameter', values)
