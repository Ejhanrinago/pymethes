#  Copyright (c) 2020 ETH Zurich

"""
Tests for the Electrons class.
"""

# Import Packages
import pytest
import numpy as np
from copy import copy

# Import modules
import pyMETHES.utils as utils
from pyMETHES.electrons import Electrons

np.seterr(all='raise')


@pytest.fixture(scope='module')
def electrons_and_field():
    """
    Returns: Electrons with 1e3 electrons, initial position (0,0,0) and std (1,1,0)
    """

    e_field = 3
    return Electrons(1000, [0, 0, 0], [1, 1, 0], e_field), e_field


def test_instantiation():

    init_n = int(1e6)
    pos = [0, 1, -2]
    std = [1, 3,  4]
    e_field = 3
    electrons = Electrons(init_n, pos, std, e_field)

    assert electrons.position.shape == (3, init_n)
    assert electrons.velocity.shape == (3, init_n)
    assert electrons.acceleration.shape == (3, init_n)
    assert electrons.num_e == init_n

    assert np.isclose(electrons.mean_position, np.array([pos]),
                      rtol=0.01, atol=0.01).all()
    assert np.isclose(electrons.var_position, np.array([std]) ** 2,
                      rtol=0.01, atol=0.01).all()


def test_accelerate(electrons_and_field):

    electrons = electrons_and_field[0]
    e_field = electrons_and_field[1]

    acceleration = utils.acceleration_from_electric_field(e_field)
    assert (electrons.acceleration[0:2, :] == 0).all()
    assert (electrons.acceleration[2, :] == acceleration).all()


def test_free_flight(electrons_and_field):

    electrons = electrons_and_field[0]
    p1 = copy(electrons.position)
    v1 = copy(electrons.velocity)
    a1 = copy(electrons.acceleration)
    dt = 1e-9
    electrons.free_flight(dt)
    assert (electrons.position == p1 + v1 * dt + 0.5 * a1 * dt ** 2).all()
    assert (electrons.velocity == v1 + a1 * dt).all()


def test_apply_scatter(electrons_and_field):

    electrons = electrons_and_field[0]
    e_field = electrons_and_field[1]
    p2 = np.random.rand(*electrons.position.shape)
    v2 = np.random.rand(*electrons.position.shape)
    electrons.apply_scatter(p2, v2, e_field)
    assert (electrons.position == p2).all()
    assert (electrons.velocity == v2).all()


def test_cached_properties(electrons_and_field):

    electrons = electrons_and_field[0]

    assert electrons._mean_position is None
    assert electrons._var_position is None
    assert electrons._mean_velocity is None
    assert electrons._mean_velocity_moment is None
    assert electrons._velocity_norm is None
    assert electrons._max_velocity_norm is None
    assert electrons._energy is None
    assert electrons._std_energy is None
    assert electrons._mean_energy is None
    assert electrons._max_energy is None
    assert electrons._acceleration_norm is None
    assert electrons._max_acceleration_norm is None
    assert electrons._energy_distribution.eedf is None

    assert electrons.mean_position is not None
    assert electrons.var_position is not None
    assert electrons.mean_velocity is not None
    assert electrons.mean_velocity_moment is not None
    assert electrons.velocity_norm is not None
    assert electrons.max_velocity_norm is not None
    assert electrons.energy is not None
    assert electrons.mean_energy is not None
    assert electrons.std_energy is not None
    assert electrons.max_energy is not None
    assert electrons.acceleration_norm is not None
    assert electrons.max_acceleration_norm is not None
    assert electrons.energy_distribution.eedf is not None

    assert electrons._mean_position is not None
    assert electrons._var_position is not None
    assert electrons._mean_velocity is not None
    assert electrons._mean_velocity_moment is not None
    assert electrons._velocity_norm is not None
    assert electrons._max_velocity_norm is not None
    assert electrons._energy is not None
    assert electrons._mean_energy is not None
    assert electrons._std_energy is not None
    assert electrons._max_energy is not None
    assert electrons._acceleration_norm is not None
    assert electrons._max_acceleration_norm is not None
    assert electrons._energy_distribution.eedf is not None

    electrons.reset_cache()

    assert electrons._mean_position is None
    assert electrons._var_position is None
    assert electrons._mean_velocity is None
    assert electrons._mean_velocity_moment is None
    assert electrons._velocity_norm is None
    assert electrons._max_velocity_norm is None
    assert electrons._energy is None
    assert electrons._mean_energy is None
    assert electrons._std_energy is None
    assert electrons._max_energy is None
    assert electrons._acceleration_norm is None
    assert electrons._max_acceleration_norm is None
    assert electrons._energy_distribution.eedf is None


def test_plot(electrons_and_field):

    electrons = electrons_and_field[0]
    electrons.plot_position(show=True, block=False)
    electrons.plot_velocity(show=True, block=False)
    electrons.plot_energy(show=True, block=False)
    electrons.plot_all(show=True, block=False)
