#  Copyright (c) 2020 ETH Zurich

"""
Tests for the Config class.
"""

# Import Packages
import pytest
import numpy as np
import scipy.constants as csts

# Import modules
from pyMETHES.config import Config

np.seterr(all='raise')


@pytest.fixture(scope='module')
def config(tmp_path_factory):
    cfg = Config('tests/data/config/test_config.json5')
    cfg.output_directory = str(tmp_path_factory.mktemp('results'))
    return cfg


def test_config():
    # reading from a json5 file
    cfg = Config('tests/data/config/test_config.json5')

    # reading from a json file
    cfg2 = Config('tests/data/config/test_config.json')

    # invalid extension
    with pytest.raises(ValueError):
        Config('invalid.config')

    # exporting to dictionary
    cfg_dict = cfg.to_dict()
    cfg_dict2 = cfg2.to_dict()
    assert isinstance(cfg_dict, dict)
    assert cfg_dict == cfg_dict2

    # check the values
    assert cfg._gas_number_density is None
    n = cfg.pressure / (csts.Boltzmann * cfg.temperature)
    assert np.isclose(cfg.gas_number_density, n)
    assert np.isclose(cfg._gas_number_density, n)

    # check that the cache is reset:
    cfg.pressure = 1e3
    assert cfg._gas_number_density is None
    n = cfg.pressure / (csts.Boltzmann * cfg.temperature)
    assert np.isclose(cfg.gas_number_density, n)
    assert np.isclose(cfg._gas_number_density, n)

    # check that the cache is reset:
    cfg.temperature = 295
    assert cfg._gas_number_density is None
    n = cfg.pressure / (csts.Boltzmann * cfg.temperature)
    assert np.isclose(cfg.gas_number_density, n)
    assert np.isclose(cfg._gas_number_density, n)


def test_save(config):
    config.save_json(f"{config.output_directory}/cfg.json")
    config.save_json5(f"{config.output_directory}/cfg.json5")
