Dummy data to test the package 'pyMETHES', formatted in accordance with the cross section data format of LXCat, www.lxcat.net

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
DATABASE: Dummy database
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

********************************

ELASTIC
notaformula
1.24e-5
PROCESS: Elastic collision
COLUMNS: Energy (eV) | Cross section (m2)
-----------------------------
 1.000000e+0	1.000000e-20
 2.000000e+0	1.000000e-20
-----------------------------

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
