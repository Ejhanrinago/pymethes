#  Copyright (c) 2020 ETH Zurich

"""
Tests for the MonteCarlo class.
"""

# Import Packages
import numpy as np
import scipy.constants as csts
import scipy.interpolate

# Import modules
import pytest
import pyMETHES.utils as utils
from pyMETHES.config import Config
from pyMETHES.monte_carlo import MonteCarlo
from pyMETHES.gas_mixture import GasMixture

np.seterr(all='raise')


@pytest.fixture(scope='module')
def config():
    return Config('tests/data/config/test_config.json5')


@pytest.fixture(scope='module')
def gas_mixture():
    config = Config('tests/data/config/test_config.json5')
    gm = GasMixture(config.gases,
                    config.paths_to_cross_section_files,
                    config.fractions,
                    config.max_cross_section_energy)
    return gm


def test_instantiation(config):

    MonteCarlo(config)


def test_scattering_angles():

    energy = np.random.random(1000)*10000
    cos_chi, sin_chi, cos_phi, sin_phi = MonteCarlo.scattering_angles(energy, True)

    # all values should be between -1 and 1.
    all_cos_sin = np.concatenate([cos_chi, sin_chi, cos_phi, sin_phi])
    assert np.logical_and(-1 < all_cos_sin, all_cos_sin < 1).all()

    # phi is uniform
    assert np.isclose(np.mean(cos_phi), 0, rtol=0.01, atol=0.1)
    assert np.isclose(np.mean(sin_phi), 0, rtol=0.01, atol=0.1)

    # cos(chi) is uniform, not chi
    assert np.isclose(np.mean(cos_chi), 0, rtol=0.01, atol=0.1)

    cos_chi, sin_chi, cos_phi, sin_phi = MonteCarlo.scattering_angles(energy, False)

    # chi should be small angles
    assert np.mean(cos_chi) > 0.65
    assert np.mean(sin_chi) < 0.45


def test_unit_scattered_velocity():

    energy = np.array([5, 8])

    vec = np.array([[1, 2],
                    [4, 5],
                    [7, 3]])

    # check that the velocity is normalized
    nor = MonteCarlo.unit_scattered_velocity(energy, vec, True)[0]
    for row in nor.T:
        assert np.isclose(np.sqrt(np.sum(row**2)), 1)

    # check that the velocity is normalized
    nor = MonteCarlo.unit_scattered_velocity(energy, vec, False)[0]
    for row in nor.T:
        assert np.isclose(np.sqrt(np.sum(row ** 2)), 1)


def test_calculate_max_coll_freq(config, gas_mixture):
    mc = MonteCarlo(config)
    mc.calculate_max_coll_freq(gas_mixture)
    assert np.all(mc.max_coll_freq.x == gas_mixture.energy_vector)
    assert np.all(np.isclose(mc.max_coll_freq.y, np.array(
        [1.17418090e+13, 1.17418090e+13, 1.37703529e+13, 7.85030898e+13])))


def test_determine_timestep(config):
    mc = MonteCarlo(config)
    x = np.linspace(0, config.max_cross_section_energy, 10)
    y = 6e-20 / np.sqrt(x + 1e-3)
    mc.max_coll_freq = scipy.interpolate.interp1d(x, y)
    mc.max_coll_period = 0.5 * csts.electron_mass / csts.elementary_charge / y
    mc.max_coll_period_squared = \
        0.5 * csts.electron_mass / csts.elementary_charge / y ** 2
    max_v = np.random.random(1000) \
        * utils.velocity_from_energy(config.max_cross_section_energy)
    max_a = np.random.random(1000) * 1000 * 1e-21 \
        * (csts.elementary_charge / csts.electron_mass)
    for v, a in zip(max_v, max_a):
        step = mc.determine_timestep(v, a)
        e_end = min(utils.energy_from_velocity(v + a * step),
                    config.max_cross_section_energy)
        assert mc.max_coll_freq(e_end) < mc.trial_coll_freq


def test_determine_collisions(config, gas_mixture):
    mc = MonteCarlo(config)
    energy = np.random.random(100)
    velocity = utils.velocity_from_energy(energy)
    mc.trial_coll_freq = 1e10

    mc.determine_collisions(gas_mixture, velocity, energy)

    assert mc.collision_by_electron.size == 100
    assert (mc.collision_by_electron <= gas_mixture.number_of_cross_sections).all()
    assert (mc.collision_by_electron >= 0).all()


def test_perform_collisions(config, gas_mixture):
    mc = MonteCarlo(config)
    num_e = 100
    position = np.random.rand(3, num_e)
    velocity = np.random.rand(3, num_e)
    energy = np.random.random(num_e)
    mc.collision_by_electron = \
        np.random.randint(0, gas_mixture.number_of_cross_sections + 1, num_e)
    ind_ionization = [3, 6]
    ind_attachment = 7
    ind_null = gas_mixture.number_of_cross_sections
    num_ionization = np.count_nonzero(np.isin(mc.collision_by_electron, ind_ionization))
    num_attachment = np.count_nonzero(mc.collision_by_electron == ind_attachment)
    num_null = np.count_nonzero(mc.collision_by_electron == ind_null)

    p, v, nc, ni, na = mc.perform_collisions(gas_mixture, position, velocity, energy)
    assert p.shape[1] == num_e + num_ionization - num_attachment
    assert v.shape[1] == num_e + num_ionization - num_attachment
    assert nc == num_e - num_null
    assert ni == num_ionization
    assert na == num_attachment

    config.conserve = True
    # test with dominant attachment
    mc.collision_by_electron = \
        np.hstack([
            np.repeat([ind_attachment], 70),
            np.repeat([ind_ionization[0]], 10),
            np.repeat([ind_null], 20)
        ])
    num_ionization = np.count_nonzero(np.isin(mc.collision_by_electron, ind_ionization))
    num_attachment = np.count_nonzero(mc.collision_by_electron == ind_attachment)
    num_null = np.count_nonzero(mc.collision_by_electron == ind_null)
    p, v, nc, ni, na = mc.perform_collisions(gas_mixture, position, velocity, energy)
    assert p.shape[1] == num_e
    assert v.shape[1] == num_e
    assert nc == num_e - num_null
    assert ni == num_ionization
    assert na == num_attachment

    # test with dominant ionization
    mc.collision_by_electron = \
        np.hstack([
            np.repeat([ind_attachment], 10),
            np.repeat([ind_ionization[0]], 70),
            np.repeat([ind_null], 20)
        ])
    num_ionization = np.count_nonzero(np.isin(mc.collision_by_electron, ind_ionization))
    num_attachment = np.count_nonzero(mc.collision_by_electron == ind_attachment)
    num_null = np.count_nonzero(mc.collision_by_electron == ind_null)
    p, v, nc, ni, na = mc.perform_collisions(gas_mixture, position, velocity, energy)
    assert p.shape[1] == num_e
    assert v.shape[1] == num_e
    assert nc == num_e - num_null
    assert ni == num_ionization
    assert na == num_attachment
