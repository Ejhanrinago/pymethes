#  Copyright (c) 2020 ETH Zurich

"""
Test to validate the simulation output, using the Reid Ramp model at E/N = 12 Td,
 and T = 0 K. The results are compared to values in table 3 from
 K. F. Ness, J. Phys. D: Appl. Phys. 27 (1994) 1848-1861.
"""

# Import Packages
import pytest
import numpy as np
import scipy.constants as csts

# Import modules
from pyMETHES.simulation import Simulation

np.seterr(all='raise')


@pytest.mark.validation
def test_reid_ramp():
    sim = Simulation('tests/data/config/reid_ramp.json5')
    mass_ratio = 2 * csts.electron_mass / (4 * csts.proton_mass)
    sim.gas_mixture.mass_ratios = np.array([mass_ratio, 0])

    sim.run()
    assert np.isclose(sim.output.energy_distribution.energy_mean,
                      0.2689, atol=0, rtol=0.01)
    assert all(np.isclose(sim.output.flux.w, [0, 0, 6.838e4], atol=400, rtol=0.01))
    assert all(np.isclose(sim.output.flux.DN, [1.135e24, 1.135e24, 0.5688e24],
                          atol=0, rtol=0.08))
